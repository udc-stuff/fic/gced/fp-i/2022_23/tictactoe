import random

def player1():
    return 'X'

def player2():
    return 'O'

def empty_cell():
    return '.'

def create_board(size = 3):
    board = []
    
    for row in range(size):    
        l = []
        for col in range(size):
            l.append(empty_cell())
        board.append(l)
        
    return board

def show_board(board):

    print("\n  ", end="")

    for cnt in range(len(board[0])):
        print(chr(ord('A') + cnt), end=" ")
    print()

    cnt = 0
    for row in board:
        cnt += 1
        print(cnt, end=' ')
        for col in row:
            print(col, end=" ")
        print()
    print()

def is_winner(board, player):
    
    # comprobando por filas y columnas
    for row in range(3):
        check_row = True
        check_col = True
        for col in range(3):
            if board[row][col] != player: # filas
                check_row = False
            if board[col][row] != player: # columnas
                check_col = False
        if check_row:
            return True
        if check_col:
            return True
        
    # diagonales
    check_main = True
    check_sec = True
    for cnt in range(3):
        if board[cnt][cnt] != player: # principal
            check_main = False
        if board[cnt][2-cnt] != player: # secundaria
            check_sec = False
    
    return check_main or check_sec

def human_to_index(s, size=3):
    l = s.split(',')
    if len(l) != 2:
        return None

	# indice de las filas
    if not l[0].isdecimal():
        return None

    r = int(l[0]) - 1
    if r < 0 or r >= size:
        return None

	# indice de las columnas
    if len(l[1]) != 1:
        return None

    c = ord(l[1].upper()) - ord('A')
    if c < 0 or c >= size:
        return None

	# r y c tienen indices validos
    return (r, c)

def full(board):
    for row in board:
        for pos in row:
            if pos == empty_cell():
                return False
            
    return True

def insert_piece(board, pos, player):
    r = pos[0]
    c = pos[1]
    if board[r][c] != empty_cell():
        return False
    else:
        board[r][c] = player
        return True
        

if __name__ == "__main__":
    SIZE = 3
    board = create_board()
    
    d = {True: player1(), False: player2()}
    
    turn = False
    while not is_winner(board, d[turn]) and not full(board):
        turn = not turn

        show_board(board)
        print("Turno del jugador", d[turn])
        
        if turn:
            pos = None
            while pos is None:
                s = input('Posicion de la ficha (formato: fila,columna)')
                pos = human_to_index(s)
                if pos is not None and not insert_piece(board, pos, d[turn]):
                    print('La posicion', pos, 'esta ocupada')
                    pos = None
        else:
            success = False
            while not success:
                r = random.randrange(0, SIZE)
                c = random.randrange(0, SIZE)
                print(f'Turno del ordenador ({r}, {c})')
                if insert_piece(board, (r, c), d[turn]):
                    success = True
        
    
    print("Fin de partida")
    show_board(board)
    if is_winner(board, d[turn]):
        print('ha ganado el juagador', d[turn])
    else:
        print("TABLAS")
    
    """
    print("Tablero vacio")
    show_board(board)
    print("Jug 1", player1(), is_winner(board, player1()))
    print("Jug 2", player2(), is_winner(board, player2()))
    
    print("Jugador 1 ha ganado por filas")
    board[1][0] = board[1][1] = board[1][2] = player1() 
    show_board(board)
    print("Jug 1", player1(), is_winner(board, player1()))
    print("Jug 2", player2(), is_winner(board, player2()))
    
    print("Jugador 1 ha ganado por columnas")
    board = create_board()
    board[0][1] = board[1][1] = board[2][1] = player1() 
    show_board(board)
    print("Jug 1", player1(), is_winner(board, player1()))
    print("Jug 2", player2(), is_winner(board, player2()))
    
    print("Jugador 1 ha ganado por la diag. principal")
    board = create_board()
    board[0][0] = board[1][1] = board[2][2] = player1() 
    show_board(board)
    print("Jug 1", player1(), is_winner(board, player1()))
    print("Jug 2", player2(), is_winner(board, player2()))
    
    print("Jugador 1 ha ganado por la diag. secundaria")
    board = create_board()
    board[0][2] = board[1][1] = board[2][0] = player1() 
    show_board(board)
    print("Jug 1", player1(), is_winner(board, player1()))
    print("Jug 2", player2(), is_winner(board, player2()))
    
    print('Casi gana el jugador 1')
    
    print("Jugador 1 ha ganado por la diag. secundaria")
    board = create_board()
    board[0][2] = board[2][0] = player1() 
    board[1][1] = player2()
    show_board(board)
    print("Jug 1", player1(), is_winner(board, player1()))
    print("Jug 2", player2(), is_winner(board, player2()))
    """
    
